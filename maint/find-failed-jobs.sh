#!/bin/bash
####################################################################
##                                                                ##
##    find-failed-jobs.sh                                         ##
##                                                                ##
##    Create symlinks to failed jobs in <jobdir>/00_failed        ##
##                                                                ##
##    Author:    Kevin Ernst                                      ##
##    Date:      June 15, 2015                                    ##
##                                                                ##
####################################################################

ALLJOBS=`mktemp --tmpdir=. cressjobsXXXXXX.lst`
HAVERESULTS=`mktemp --tmpdir=. cressjobsXXXXXX.lst`

LABROOT=/data/weirauchlab
JOBSDIR=$LABROOT/web/data/cressint

quietly() {
    $* &>/dev/null
}

fawk() {
    awk -F/ "{ print \$$1 }"
}

# Courtesy of 'update-databank-metadata.sh'
dir_ok() {
    mountpoint="$1"
    read -t1 < <(stat -t "$mountpoint" 2>&-)
    if   [ -z "$REPLY" ];                   then return 1; fi 
    if ! [ -d "$1" -a -r "$1" -a -x "$1" ]; then return 1; fi
    return 0                                # otherwise
}

bail() {
    echo "$ME: $1" >&2
    #for_length_of $ME ' ' >&2
    echo "Quitting." >&2
    exit ${2:-1}               # bail w/ user specified exit code or 1
}

#trap "quietly rm -f $ALLJOBS $HAVERESULTS" 0 1 2 15

if ! dir_ok $JOBSDIR; then
    bail 'Problems reading a required directory'
fi

quietly pushd $JOBSDIR
find ./* -maxdepth 0 -type d ! -name "00_*" > $ALLJOBS
find . -path "./00_*" -prune -o -name "allresults.bed" > $HAVERESULTS
quietly popd
