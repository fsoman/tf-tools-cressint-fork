<?php
// FIXME: This tool should also probably just be an Ajax call from 'rlist.php'

require_once('/var/www/lib/php/WRL/tfinit.php');
require_once('/var/www/lib/php/WRL/tabtable.php');
require_once('/var/www/lib/php/WRL/tfclusterjob.php');

$ini_file  = tf_find_ini();
$job_id    = preg_replace('/[^a-f0-9]/', '', $_GET['j']);
$pbmfile   = 'PBMresults.bed';
$cfg       = NULL;
$matches   = NULL;
$errmsg    = '';

if ( $ini_file === false ) {
  tf_bail("Problem reading INI file and/or rendering index."); 
}
tf_read_config($cfg, $ini_file, array( 'job_id' => $job_id ));

$tool_name   = $cfg['global']['tool_name'];
$output_path = $cfg[$tool_name]['output_path'];
$log_dir     = $cfg['cluster']['bsub_log_path'];


if ( isset($_SERVER['HTTP_REFERER']) ) {
  $referrer = $_SERVER['HTTP_REFERER'];
} else {
  // FIXME: this will bomb on the dev server
  $referrer = "https://tf.cchmc.org/$tool_name/results/$job_id";
}

// Attempt to change to the output directory path
chdir($output_path) or tf_bail("Error opening output directory.");

if (!file_exists($pbmfile)) {
  // FIXME: This isn't visible in the client.
  header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
  header("Refresh: 5; $referrer");
  tf_bail(<<<"HTML"
    <p>The filename <tt>$pbmfile</tt> could not be found in
       the output directory for <tt>$tool_name</tt>.</p>
    <!--p>Returning you to the results page in 5 seconds, or
       <a href="$referrer">click here</a>.</p-->
HTML
  );
}
else {
  header('Content-Type: text/html');
  $id = escapeshellarg($_GET['id']);
  #echo var_dump($id);
  $matchpbms = explode("\n", shell_exec("grep $id PBMresults.bed"));
  #echo var_dump($matchpbms);
  echo implode("\n", tabs_to_table($matchpbms, false));
} // if the file exists (or doesn't)

// vim: tags+=/var/www/lib/php/WRL/tags
