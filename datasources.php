<?php
// ========================================================================
//
//  CressInt  datasources.php
//
//  Authors:   K. Ernst, F. Soman
// 
//
// ========================================================================

require_once('/var/www/lib/php/WRL/tfinit.php');
require_once('/var/www/lib/php/WRL/tftemplate.php');

$ini_file = tf_find_ini();  // open toolname.ini in c.w.d.

if ( !tf_template_init_from_ini($ini_file, $form_key) ) {
  tf_bail("Problem reading config file \"$ini_file\" and/or rendering index.");
}

global $TWIGVARS;  // the tf_template_init*'s create this variable hash
global $TWIG;      // ...and this Twig environment
echo $TWIG->render('datasources.html.twig', $TWIGVARS);

// vim: set tags+=/var/www/lib/php/WRL/tags
