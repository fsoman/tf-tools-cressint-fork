# CressInt: _A. thaliana_ genomic analysis tool #

![](img/cressint_sidebar_hidden.png)

##Abstract##

The thale cress, [_Arabidopsis thaliana_][wp1], is a model organism for
studying a wide variety of biological processes. Recent advances in sequencing
technology have resulted in a wealth of information describing numerous
aspects of genome function. However, there are currently very few
computational systems for effectively using these data to create testable
hypotheses.

[CressInt][ci] is a web resource for exploring gene regulatory
mechanisms in _A. thaliana_ on a genomic scale. CressInt incorporates
a variety of genome-wide data types relevant to gene regulation, including
transcription factor (TF) binding site models, ChIP-seq, DNase-seq, eQTLs, and
GWAS.

By compiling the output of several different console-based analysis tools
running on CCHMC's [HPC cluster][bmi] in a user-friendly way, CressInt aims to
provide insights into _A. thaliana_'s gene regulation without the need for
researchers to create their own command-line analysis pipelines from scratch.

Examples uses of CressInt are:

1. identifying TFs binding to the promoter of a gene of interest
2. identifying genetic variants that are likely to impact TF binding based on
    a ChIP-seq dataset; and
3. identifying specific TFs whose binding might be impacted by
    phenotype-associated variants.

The manuscript associated with CressInt, "CressInt: a user-friendly web
resource for genome-scale exploration of gene regulation in Arabidopsis
thaliana," was accepted for publication in _Current Plant Biology_ in
2015. The public web interface for the tool is [https://cressint.cchmc.org](https://cressint.cchmc.org).

## Implementation ##

The user interface to the CressInt analysis pipeline is served by a GNU/Linux
virtual machine running CentOS 6 and the Apache 2.2 web server. The web
front-end is implemented primarily as HTML "templates" rendered through the
use of the [Twig][tw] PHP library, which maintains a separation of concerns
between interface and application logic. The form submission is done via Ajax
(see `js/formsubmit.js`), allowing certain types of validation errors such as
missing inputs or malformed BED files to be detected and reported without
a page reload.

Input data for analysis is received and processed by a Perl CGI script
(`cressint.cgi`), which in turn interfaces with an in-house HPC cluster
comprised of over 700 processing cores through a set of locally-developed Perl
modules, generating shell scripts for batch processing. These Perl modules
abstract away the implementation details of the batch facility (IBM’s LSF) and
allow interfaces to be written for other local or remote HPC load-sharing
systems without impacting the front-end web service.

Intersection analyses are performed using the [bedtools suite][bt], along with
custom-written code written in C++. (It is our intention to release the source
code for the full backend pipeline at a later time.)

## Usage ##

CressInt will accept _A. thaliana_ gene names (from the TAIR [gene
aliases][ga] list) and is fairly liberal in accepting BED4-ish genomic
coordinates. See the [Help & FAQs](faq) page on the public site for more
information about acceptable input formats. The user can also select from
a set of phenotypes of interest taken from a recent [GWAS study][gwa].

The user can also choose to include or exclude functional genomics datasets
based on data or tissue type. After error and format checking, CressInt
converts the input into a set of labeled genomic coordinates (in BED format)
and intersects these coordinates with the selected datasets.  Two sets of
results are presented to users: (1) the intersection results, which indicate
all data sets in the system whose coordinates overlap with the input set; and
(2) TF differential binding predictions, which identify genetic variants that
might impact the binding of specific TFs.

A user may optionally provide an email address to receive notification of
a completed CressInt analysis, or may simply leave the web browser open and
wait for the job to complete. Average sized jobs (no more than a few hundred
coordinates) typically complete in within minutes; very small jobs
consisting of a single gene or just a handful of coordinates will completely
nearly instantaneously, depending on cluster load.

_Screenshots showing the [analysis results page](img/cressint_sidebar_hidden.png)
and the [input form in Mode 1](img/cressint_mode1.png) (intersect coordinates)
may be found in the `img` subdirectory of this repository._

### Sample output ###

See the `sample_output` folder in the source repository (or online at the
public web site [here][so]) for example input and analysis output for each of
the three modes.

## Contributing ##

If you'd like to contribute to the project, drop us a line. If you find
a minor problem, always feel free to [fork the Bitbucket repository][bbf] and
fix it, then file a merge/pull request. You may contact the developers
directly using the contact email below.

To be perfectly honest, we'd like to do a better job of following [PSR-2][psr]
with our _own_ code (and feel free to call us out on particularly flagrant
violations). However, we're really sticklers about the line length and
indentation requirements (4-space indents, line length <80 characters).

## References ##

* Admin contact: [tftoolsadmin -at- cchmc.org][mt]
* [Internal documentation][ciw] (on WRL wiki)

CressInt is a [Weirauch Transcription Factor Research Lab][wrl] production.

[ci]: https://cressint.cchmc.org
[bmi]: https://bmi.cchmc.org/resources/clusters
[tw]: http://twig.sensiolabs.org/
[bt]: https://github.com/arq5x/bedtools2
[ga]: https://www.arabidopsis.org/download_files/Genes/gene_aliases_20130831.txt
[faq]: https://cressint.cchmc.org/cressint/faq.php
[gwa]: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3023908/
[so]: https://cressint.cchmc.org/cressint/sample_output/
[bbf]: https://bitbucket.org/weirauchlab/tf-tools-cressint/fork
[psr]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md
[wp1]: https://en.wikipedia.org/wiki/Arabidopsis_thaliana
[wrl]: http://www.cincinnatichildrens.org/research/divisions/a/genomics-etiology/labs/weirauch/default/
[ciw]: https://tfwiki.cchmc.org/wiki/TF-CressInt
[mt]: mailto:tftoolsadmin-at-cchmc.org

