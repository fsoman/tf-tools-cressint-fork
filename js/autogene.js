$(function() {
  'use strict';
  var listBox   = $('#coords');
  var searchBox = $('#find-gene-names');

  function addGene(geneName) {
    var val;
    var ary = [];
    if (typeof(geneName) === 'undefined') { return; }

    // In case the user somehow manages to paste in carriage returns
    // Also trim whitespace and leading/trailing spaces
    /*jsl:ignore*/
    val = listBox.val().replace(/(\r\n|\n|\r){2,}/gm, '\n')
                       .replace(/ +/gm, '')
                       .trim();
    /*jsl:end*/

    // Add a newline only if we need it
    if ( val.length > 0 && val[val.length-1] !== '\n' ) { val += '\n'; }
    val += geneName;

    // Now remove duplicates:
    ary = val.split(/\n/).filter(function(e,i,a) { return a.indexOf(e)==i; });

    // Update the text input field and add a new line for the user
    listBox.val( ary.join('\n') + '\n' );
    listBox.scrollTop(listBox[0].scrollHeight);

    // Clear the listbox (this short delay seems to be necessary for jQuery to
    // let its hooks out of the autocomplete control)
    window.setTimeout(function(){searchBox.val('');}, 10);
  } // addGene()

  searchBox.autocomplete({
    source:    "tairgenes.php",
    minLength: 2,
    select:    function(event, ui) {
      addGene( ui.item ? ui.item.value : undefined );
    }
  });
});

