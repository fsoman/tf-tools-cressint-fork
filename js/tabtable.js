'use strict';

/**
 * @function $.fn.textWidth
 * @brief jQuery plugin to compute an element's width by rendering it into an
 * invisible <pre>span</pre> element and measuring its size.
 *
 * @param   $(this)
 * @return  the element width in pixels
 *
 * Courtesy: 
 * http://object-space.blogspot.com/2012/05/truncating-long-text-in-table-cells.html
 */
$.fn.textWidth = function() {
  var txt = this.html();
  var n = $('<span class="widthcheck">'+txt+'</span>');
  n.css('font-family', this.css('font-family'));
  n.css('font-size', this.css('font-size'));
  $('body').append(n);
  var width = n.width();
  n.remove();
  return width;
}; // jQuery plugin 'textWidth'


function assignTooltipClass(elem) {
  if (typeof $.tooltipsAssigned === 'undefined') {
    var id = '#' + elem[0].id;
    // We specify 'title=' attributes for <th>s, so don't clobber these:
    //var selector = id + ' th, ' + id + ' td';
    var selector = id + ' td';
    // Compute which cells will need tooltips:
    //$('#message').text('Calculating cell widths...');
    $(selector).each(function() {
      if ( $(this).textWidth() > $(this).width() ) {
        $(this).addClass('tip');
      }
    });
    $.tooltipsAssigned = true;
    //$('#message').text('');
    return true;
  }
  return false;
} // assignTooltipClass


function addTooltips(elem) {
  // If column headers would be truncated, put contents back in a 
  // tooltip 
  assignTooltipClass(elem);
  $('.tip').each(function() {
    $(this).attr('title', $(this).text());
  });
  elem.addClass('tipped');
} // addTooltips


function removeTooltips(elem) {
  $('.tip').each(function() {
    $(this).removeAttr('title');
  });
  elem.removeClass('tipped');
} // removeTooltips


// Courtesy: http://stackoverflow.com/a/10000178
function tieToggleButton(btn, elem, cls, labels) {
  btn.click(function(e) {

    if (elem.hasClass(cls)) {
      // Remove it, and remove the tooltips
      elem.removeClass(cls);
      btn.text(labels.onLabel);
      btn.attr('title', labels.onTitle); //.button('refresh');
      removeTooltips(elem);
    }
    else {
      // Add the class 'cls' and add tooltips
      elem.addClass(cls);
      btn.text(labels.offLabel);
      btn.attr('title', labels.offTitle); //.button('refresh');
      addTooltips(elem);
    } // if the element has class 'cls'
  });
} // add click handler to the toggle button

