// Helper function to wire up the event handler for the button that hides the
// "first launch" help at the top of the input form.
var FirstLaunch = FirstLaunch || (function() {
  'use strict';
  var cookie = 'cressint-firstlaunch';  // "first launch" => 1 if already shown

  function __wireUpHideButton(btnId, helpMsgId) {
    $('#'+btnId).click(function(e) {
      e.preventDefault();
      Cookies.set(cookie, 1);
      //$(this).parent().hide(400);
      $('#'+helpMsgId).hide(200);
    });
  }

  return {
    wireUp: function(btnId, helpMsgId) {
      if (Cookies.get(cookie)) {
        return false;
      } // else
      $('#'+btnId).parent().show(0);
      __wireUpHideButton(btnId, helpMsgId);
      return true;
    }
  };

}()); // FirstLaunch

(function() {
  'use strict';

  // TODO: Figure out how to suppress and then re-synthesize click events on
  // radio buttons (reliably, cross-browser) so that you CAN actually click
  // "Cancel" at the warning prompt about wiping your existing form inputs.

  var alertbox  = document.getElementById('valid-msg');
  var inputform = document.getElementById('coords');

  function confirmClearInput(e) {
    if (e.value !== '' &&
        !confirm("This will clear the input list's contents. Is that okay?")) {
      return false;
    } // otherwise
    e.value = '';
    return true;
  }

  // Reset form elements (and sections that are hidden) to a sane default
  // state:
  function resetForm(e) {
    e.preventDefault();
    dismissAlert(alertbox); // from js/formsubmit.js

    if (!confirmClearInput(inputform)) { return; }
    // otherwise
    this.form.reset();

    // Hide the phenotype selections and show the BED input field
    document.getElementById('coords-cell').style.display = 'table-cell';
    document.getElementById('pheno-cell').style.display = 'none';

    // Hide the gene name auto-complete box
    document.getElementById('find-gene-names').style.display = 'none';

    // Re-select everything in the available/selected listboxes
    // FIXME: The 
    listboxSelectAll('available-lines'); // from assets/js/formhelpers.js
    listboxCopyOver('available-lines', 'selected-lines');
    listboxSelectAll('selected-lines');
    listboxSelectAll('available-tracks');
    listboxCopyOver('available-tracks', 'selected-tracks');
    listboxSelectAll('selected-tracks');

    // Put the phenotypes description back to a sensible default (yeah, I
    // know this doesn't match what's in inputform.html.twig
    document.getElementById('pheno-explain').innerHTML = '<p>' +
      'Choose phenotypes for identification of associated SNPs ' +
      'and the transcription factors regulated by these SNPs.</p>';
  } // resetForm(e)

  // Stuff to do to set up the default states/selections for all the form
  // elements
  window.onload = function () {
    document.getElementById('resetbtn').onclick = resetForm;
    document.getElementById('resetbtn-top').onclick = resetForm;

    // Have to add this as an event listener (not .onclick) because WLAjax
    // registers an event handler on this button, too.
    // FIXME: This doesn't probably actually work because WLAjax.tieForm
    // registers its handler first. Adding this before js/formsubmit.js gets
    // loaded might help, but it's not a totally comprehensive solution.
    document.getElementById('submitbtn').addEventListener('click', function(e) {
        //e.preventDefault();
        listboxSelectAll('selected-lines');
        listboxSelectAll('selected-tracks');
        //this.form.submit();
    });

    // Need to make sure that all the items in the "Selected" cell lines and
    // tracks are actually selected before submitting.

    // Wire up the onchange event for the mode-selector radio button set:
    document.getElementById('mode1-radio').onclick = function(e) {
      document.getElementById('coords-cell').style.display = 'table-cell';
      document.getElementById('input-type-cell').style.display = 'inherit';
      document.getElementById('pheno-cell').style.display = 'none';
    };
    document.getElementById('mode2-radio').onclick = function(e) {
      document.getElementById('coords-cell').style.display = 'table-cell';
      document.getElementById('input-type-cell').style.display = 'inherit';
      document.getElementById('pheno-cell').style.display = 'none';
    };
    document.getElementById('mode3-radio').onclick = function(e) {
      document.getElementById('coords-cell').style.display = 'none';
      document.getElementById('input-type-cell').style.display = 'none';
      document.getElementById('pheno-cell').style.display = 'table-cell';
      hideIfShown('promoter'); 
    };

    // Wire up the 'onchange' events for the 'input-type' radio button set:
    // GENOMIC COORDINATES RADIO BUTTON
    document.getElementById('input-type-coords').onclick = function(e) {
      //e.preventDefault();
      if (inputform.val !== '') {
        if (!confirmClearInput(inputform)) { return; }
      } // otherwise ...
      // For some reason I don't yet understand, I can't just
      // this.dispatchEvent(e) here, and I had trouble with this.click() in
      // Chrome.
      //this.click();
      document.getElementById('lbl-input-coords').style.display = 'inline';
      document.getElementById('lbl-input-gene-name').style.display = 'none';
      document.getElementById('find-gene-names').style.display = 'none';
      document.getElementById('lbl-input-fasta').style.display = 'none';
      bindSampleData({ txtId: 'coords', btnId: 'samplebtn',
                       filename: 'data/case_study_2.txt' });
      hideIfShown('promoter'); 
    };
    // GENE NAMES RADIO BUTTON
    document.getElementById('input-type-names').onclick = function(e) {
      //e.preventDefault();
      if (inputform.val !== '') {
        if (!confirmClearInput(inputform)) { return; }
      } // otherwise ...
      //this.click();
      document.getElementById('find-gene-names').style.display = 'inline';
      document.getElementById('lbl-input-gene-name').style.display = 'inline';
      document.getElementById('lbl-input-coords').style.display = 'none';
      document.getElementById('lbl-input-fasta').style.display = 'none';
      bindSampleData({ txtId: 'coords', btnId: 'samplebtn',
                       filename: 'data/case_study_1.txt' });
      showIfHidden('promoter'); 
    };
    // FASTA SEQUENCE(S) RADIO BUTTON
    document.getElementById('input-type-fasta').onclick = function(e) {
      //e.preventDefault();
      if (inputform.val !== '') {
        if (!confirmClearInput(inputform)) { return; }
      } // otherwise ...
      //this.click();
      document.getElementById('lbl-input-fasta').style.display = 'inline';
      document.getElementById('lbl-input-gene-name').style.display = 'none';
      document.getElementById('find-gene-names').style.display = 'none';
      document.getElementById('lbl-input-coords').style.display = 'none';
    };

    // Change the help text for the upstream/downstream fields based on
    // whether the "Coordinates relative to TSS?" checkbox is checked:
    var promchk = document.getElementById('scan-promoter');
    var promdefaultmsg = document.getElementById('rel-to');
    // Force 'Coordinates relative to TSS?' to be unchecked on page load
    promchk.checked = false;
    promdefaultmsg.textContent = 'gene body';
    promchk.onclick = function(e) {
      promdefaultmsg.textContent = promchk.checked ? 'TSS' : 'gene body';
    };

    // Force the "Mode 1" and "Input type" -> "Gene coordinates" radio buttons
    // to be selected (Firefox has a tendency to retain form state between page
    // reloads, which is fine, except in this case):
    document.forms['inputform'].elements['mode-selector'].value = 'mode1';
    document.forms['inputform'].elements['input-type-selector'].value = 'coords';

    // Bind 'samplebtn' to load sample data from 'sampledata.bed'
    // (/assets/js/sampledata.js):
    bindSampleData({
      txtId:    'coords',
      btnId:    'samplebtn',
      filename: 'data/case_study_2.txt'
      //defaults: [ 'item1', 'item2', 'item3' ]
    });

    // Make the phenotype list selections "title" attributes show up in the
    // #pheno-explain div:
    var phlst = document.getElementById('phenotypes');
    phlst.onchange = function(e) {
      var msg = '';
      if (phlst.selectedOptions.length > 1) {
        msg = '<em>Multiple phenotypes selected (' +
              phlst.selectedOptions.length + ')</em>';
      } else {
        var ph = phlst.children[phlst.selectedIndex];
        msg = '<strong>' + ph.value + ':</strong> ' +
              phlst.children[phlst.selectedIndex].title;
      }
      document.getElementById('pheno-explain').innerHTML = '<p>' +msg+ '</p>';
    }; // phenotype select box onclick event

    // "Available" and "Selected" cell lines and tracks listboxes:
    bindListboxPair('lines');
    bindListboxPair('tracks');

    // Wire up the show/hide link for all "collapsers" on the page:
    var collapsers = document.getElementsByClassName('collapser');
    Array.prototype.forEach.call(collapsers, function(collapser) {
      Array.prototype.forEach.call(collapser.childNodes, function(el) {
        // Find the first element with an ID of 'something-toggle' and add click
        // handlers to the element ID 'something':
        if (el.id === undefined) { return; }
        var idMatch = el.id.match(/(.*)-toggle/);
        if (idMatch !== null) {
          document.getElementById(el.id).onclick = function(evt) {
            evt.preventDefault();
            showHide(idMatch[1]); // m[1] is the first captured pattern
          };
        }
      }); // for each child node
    }); // for each '.collapser' element

    // Select all lines and all tracks by default:
    listboxSelectAll('available-lines');
    listboxCopyOver('available-lines', 'selected-lines');
    listboxSelectAll('available-tracks');
    listboxCopyOver('available-tracks', 'selected-tracks');

    // Wire up the buttons for Case Study #1/2/3 (js/casestudies.js)
    CaseStudy.wireUp();

    // Show the first-launch help on the '#intro' element
    FirstLaunch.wireUp('ok-got-it', 'intro');

  }; // window.onload
}());

// cressint.js
