<!DOCTYPE html>
<!---
//*******************************************************************************//
//                                                                               //
//	CressInt Arabidopsis thaliana Intersector FASTA to BED Conversion Tool       //
//  fasta2bed.php   FASTA conversion to BED6 file                                //
//                                                                               //
//    Author:  Frances Soman                                                     //
//	                                                                             //
//*******************************************************************************//
// Functions required for converting Fasta files to BED 6 format.
// 
// Stand-alone FASTA Conversion Tool (fastaform.php,fastaform.html.twig,
//                                    fasta2bed.php,fastaresults.php)
// Cressint homepage fasta button (fasta.php, fasta2bed.php)
// Use this function for cressint homepage fasta button: f2b($fastafile, $fasta, $calledby)
// Output: BED6 format file 
// INPUT:
// $fastafile is a fasta sequence file; $fasta is a fasta sequence string
// $fdisplay is an array which is returned from f2b for use in f2b_display for 
//  the user conversion display web page
// $calledby = 0   The fastaform user specified FASTA file and FASTA sequence(s).
// $calledby = 1   The fastafile is the path and filename on the server of 
//                 the FASTA file (with already sanitized file name) and 
//                 fasta is the FASTA sequence(s) string
//IMPORTANT:  Do the following to include this tool in the Weirauch Lab
//            Cressint TWIG environment: Remove the comments from the TWIG statements, lines 2,44,48,55,898, 903
//            Remove the comments from the Cressint $cfg global statements: 
//			       lines 165,242-267,308-318
//            REMOVE the statements (including GLOBAL statements): 
//				   lines 163,166,167,269-288,309,319-328,337,340,349
//			  REMOVE the following comment bars for require_once: lines 33-35 
//require_once('/var/www/lib/php/WRL/tfinit.php');
//require_once('/var/www/lib/php/WRL/tftemplate.php');
//require_once('/var/www/lib/php/WRL/tfconfig.php'); 

 {# /var/www/twig/tftool.html.twig #}
{% extends 'tftool.html.twig' %}
{% import 'assets.twig' as assets %}

{% block headassets %}
{# All the regular CSS and JavaScript assets #}
{{ parent() }}
{{ assets.inputform_assets() }}--->
  <link rel="stylesheet" type="text/css" href="css/cressint.css">
  <link rel="stylesheet" type="text/css" href="css/facressint.css">

<!----  
{% endblock %}

{% block nav %}
{% include 'sidebar.html.twig' %}
{% endblock %}

{% block main %}--->
<html>

<head>
<div>
.
</div>
<body>
<div class="f">
<h1>FASTA to BED Conversion</h1>
<div class="cr_format">

<?php
{
$calledby = 0;
$fastafile = "";
$fasta = "";
$fdisplay = array();
$GLOBALS['bed_err'] = array();
$GLOBALS['bed_errcount'] = 0;
	
	f2b($fastafile, $fasta, $fdisplay, $calledby);
	if (!empty($bed_err))	{
		echo "There seems to be a problem converting the FASTA.";
		return(FALSE);
	}
			
	f2b_display($fdisplay);
}
	 		
function f2b_display($fdisplay)	{	
	?>
	<p>
	<label>Converted BED File:</label>
	<?php
	$f = 0;
	$b = 0;
	$p_user_fasta = $fdisplay[0];
	$p_user_bed = $fdisplay[1];
	$user_fasta = $fdisplay[2];
	$user_bed = $fdisplay[3];
	$c_user_fasta = $fdisplay[4];	
	$c_user_bed = $fdisplay[5];	
	$options = array();
	
	if (!file_exists($p_user_fasta))	 {
		$f = 1;
	}
	if (!file_exists($p_user_bed))	 {
		$b = 1;  
		echo "Error displaying BED file " . $user_bed;
	}
	else	{
		if ($f == 0) {
			?>
			<span class="q1 q2">
			<label><?php echo $user_fasta; ?> </label>
			<a href='fastaresults.php?var1=<?php echo $c_user_fasta; ?>&var2=<?php echo $user_fasta; ?>' target='_blank' title="Download processed FASTA file" >Download FASTA</a>
			</span>
			<?php
			
		}
		else {     //No FASTA file to display
			?>
			<span class="q1 q2">
			<label><?php echo $user_fasta; ?> not available for display. </label>
			</span>
			<?php	
		}
		?>
		<span class="q1 q2">
		<label><?php echo $user_bed; ?> </label>
		<a href='fastaresults.php?var1=<?php echo $c_user_bed; ?>&var2=<?php echo $user_bed; ?>' target="_blank" title="Download BED file">Download BED</a>
		</span>		
		<?php	
		format_table($p_user_bed);
		?>
		</div>
	<?php
	
	}
	return;
}

function f2b($fastafile, $fasta, &$fdisplay, $calledby)	{
	echo "Processing FASTA for conversion.";
//Fasta2bed conversion
global $bed_err;
global $bed_errcount;

$user_psl = 'user_out.psl';
$tfiles = "";
$tempnme = "";
$rtempnme = "";
$formname = "tfastafile";
$tempuser_file = "";
$user_file = "";
$user_ffile = "";
$user_bed = "";
$user_bbed = "";
$destination = "";
$user_fasta = "";
$tuserfa = "";
$fpath = "";		
$p_faseq = 0;				
$bed_out = 'bed_out.bed';		//General BED format

// GLOBALS: For Cressint TWIG environment, use $cfg statements below
$GLOBALS["cressint"] = "/var/www/html";  //TWIGVARS cressint_path
$GLOBALS["c_path"] = "/cache";			  // TWIGVARS cressint cache path (/var/www/html/cressint/cache)
//$fpath = $cfg['cressint']['c_path'];    // Use for Cressint TWIG environment      
$fpath = $GLOBALS['cressint'] . $GLOBALS['c_path'];	// TWIGVARS cressint cache path
$GLOBALS["cressint"] = "/var/www/html";
$cpath = "./" . $GLOBALS['c_path'];   //Relative path for file display and download
$genome_file = 'araTha1.2bit';
$p_g = 'AT_10pep.fa';
$r_g = 'araTha1.2bit';
$output = array();
$ret = 0;
$cmd = '';
$hdr = array();
$hdr1 = 'BED file:';
$hdr2 = ' for FASTA file: ';
$bcols = array();
$type = "";
unset($output);
unset($ret);
unset($retvalue);

//Prepare user file
if ($calledby == 0)	{ 	//If f2b is called by fastaform
	$newname = "";
	if (($newname = check_ufile($formname, $p_faseq, $fasta, $newname)) == "1") {
		return;
	}
	$fastafile = $newname;	
	$tfiles = $_FILES['tfastafile']['tmp_name'];
	get_name($tfiles, $tempnme);
	if ($tfiles !== "")	{
		$destination = $fpath . "/" . $tempnme . $newname;
		if ((move_uploaded_file($tfiles,$destination)) !== TRUE) {
			$bed_err[$bed_errcount++] = 'faf_tmpmoverr';
			echo "There seems to be a problem processing your FASTA file " . $fastafile . ".";
			return;
		}
		else {
			if (file_exists($destination) == FALSE ) {
				$bed_err[$bed_errcount++] = 'faf_tmpmoverr';
				echo "There seems to be a problem processing the FASTA file " . $fastafile . ".";
				return;
			}
		}	
	} //if tfiles !NULL
} 
 
if ($fastafile == "")	{
	if (($p_faseq == 0) || ($fasta == "")) {
		$bed_err[$bed_errcount++] = 'no_fastafileorseq';
		echo "No FASTA file or sequence provided. Please choose a FASTA file or paste FASTA sequence(s).";
		return;
	}
}
else 	{
		if (($p_faseq == 1) || ($fasta !== ""))	{
			echo "Both FASTA file and pasted sequence found. Only your FASTA file will be processed.  Please enter pasted sequences separately.";
		}
}

$tempuser_file = $tempnme . $fastafile;
$user_ffile = $fastafile;
$user_bbed = $fastafile;
if ($fastafile == "")	{            //Sequence pasted into $fasta
	$rtempnme = mt_rand(0,10000);
	$rtempnme = $rtempnme . "_";
	$user_ffile = "user_fasta.fa";
	$user_bbed = 'user_bed.bed';
	$user_file = $rtempnme . 'user_fasta.fa';	//BED 6 format
	$user_bed = $rtempnme . 'user_bed.bed';	//BED 6 format	
}
else	{
	$user_ffile = preg_replace('/\./','_fasta.', $user_ffile);
	$user_file = $tempnme . $user_ffile;
	$user_bbed = preg_replace('/\.fa/','.bed', $user_bbed);
	$user_bed = $tempnme . $user_bbed;
}

//These need to be used in Cressint instead of the GLOBALS below
/*   
$fasta2bed = $cfg['cressint']['c_path'] . "/$user_bed";

$bedoutfile = $cfg['cressint']['c_path'] . "/$bed_out";
$bedout = $bedoutfile;

if ($calledby == 0)	{
	if ($p_faseq == 0)	{
		$tuserfilefa = $cfg['cressint']['c_path'] . "/$tempuser_file";
		$tuserfa = $tuserfilefa;
	}
	else {
		$tuserfa = $tempuser_file;
	}
}
else	{
$tuserfa = $tempuser_file;
}

$userfilefa = $cfg['cressint']['c_path'] . "/$user_file";
$userfa = $userfilefa;

$pslout = $cfg['cressint']['c_path'] . "/$user_psl";
$cressint_blat = $cfg['cressint'] . "/scripts/blat";
$cressint_next = $cfg['cressint'] . "/scripts/pslToBed";
*/

$fasta2bed = $GLOBALS['cressint'] . $GLOBALS['c_path'] . "/$user_bed";

$bedoutfile = $GLOBALS['cressint'] . $GLOBALS['c_path'] . "/$bed_out";
$bedout = $bedoutfile;

if ($calledby == 0)	{
	if ($p_faseq == 0)	{
		$tuserfilefa = $GLOBALS['cressint'] . $GLOBALS['c_path'] . "/$tempuser_file";
		$tuserfa = $tuserfilefa;
	}
	else {
		$tuserfa = $tempuser_file;
	}
}
else	{
$tuserfa = $tempuser_file;
}

$userfilefa = $GLOBALS['cressint'] . $GLOBALS['c_path'] . "/$user_file";
$userfa = $userfilefa;

//parse user fasta 
$bed_err = parse_sequence($tuserfa,$userfa, $fasta,$type);
check_user_type($type);
if ($type == "not_valid") {
	echo "Error processing FASTA Sequence.  Sequence not valid. Please check file";
	return;
}
if (!empty($bed_err))	{
	echo "Error processing FASTA Sequence, please check file.";
	$l = 0;
	$l = sizeof($bed_err);
}
else	{	
	// Arabidopsis thaliana genome fasta is here
	//USE the following for Cressint tool environment instead of GLOBALS
	/*
	if ($type = '-t=prot -q=prot')	{
		$thal_genome = $cfg['cressint'] . "/$p_g";
	}
	elseif ($type = '-t=rna -q=rna')	{
		$thal_genome = $cfg['cressint'] . "/$r_g";
	}
	else	{
		$thal_genome = $cfg['cressint'] . "/$genome_file";
	}
*/	
	if ($type == '-t=prot -q=prot')	{
		$thal_genome = $GLOBALS['cressint'] . "/$p_g";
	}
	elseif ($type == '-t=dna -q=rna')	{
		
		$thal_genome = $GLOBALS['cressint'] . "/$r_g";
	}
	else	{
		$thal_genome = $GLOBALS['cressint'] . "/$genome_file";
	}
	
	if (!file_exists($thal_genome)) {
	$bed_err[$bed_errcount++] = 'ng_noathalgenome';
	return;
	}
	
	// These are above for Cressint tool environment, with $cfg instead of GLOBALS 
	// conversion
	$pslout = $GLOBALS['cressint'] . $GLOBALS['c_path'] . "/$user_psl";

	//commands to do the conversion
	$cressint_blat = $GLOBALS['cressint'] . "/scripts/blat";
	
	if ($type !== "")	{
		$cmdb = $cressint_blat . " " .  $type . " " . $thal_genome . " " . $userfa . " " . $pslout;
	}
	else {
	$cmdb = $cressint_blat . " " . $thal_genome . " " . $userfa . " " . $pslout;
	}	
	
	$cressint_next = $GLOBALS['cressint'] . "/scripts/pslToBed";
	$cmdn = $cressint_next . " " . $pslout . " " . $bedout;
	exec($cmdb, $output, $ret);
	if ($ret !== 0)	{
		echo 'Error converting FASTA';
		$bed_err[$bed_errcount++] = 'fc_fablatconverr';
		return;
		}
	if (!file_exists($pslout)) {
		$bed_err[$bed_errcount++] = 'no_psloutfile';
		return;
		}
	if (filesize($pslout) == 0)	 {
		$bed_err[$bed_errcount++] = 'no_psloutmt';
		return;
	}
	unset($output);
	unset($ret);
	exec($cmdn, $output, $ret);
	if ($ret !== 0)	{
		echo "Error converting FASTA.";
		$bed_err[$bed_errcount++] = 'pc_p2bconverr';
		return;	
	}

	// Parse the bed file to get only BED6 format
	if (!file_exists($bedout)) {
		$bed_err[$bed_errcount++] = 'no_bedoutfile';
		return;
		}
	if (filesize($bedout) == 0)	 {
		$bed_err[$bed_errcount++] = 'no_bedoutmt';
		return;
	}

	if (($rf = fopen($bedout, "r")) !== FALSE){
		// Writing tab delimited BED 6 file, user bed file
		if (($wf = fopen($fasta2bed,"w")) == TRUE) { 
			$bed_write = TRUE;
			$hdr = $hdr1 . $user_bbed . $hdr2 . $user_ffile . PHP_EOL;
			if (fwrite($wf, $hdr) == FALSE) {
				$bed_err[$bed_errcount++] = 'w_bed6_hdr';     
			} 
			$bcols = array("Chr","Start","End","Name","Score","Strand");
			if (fputcsv($wf, $bcols, "\t") == FALSE) {
				$bed_err[$bed_errcount++] = 'w_bed6_colhdrs';     
			}
			if (($line = fgets($rf)) !== false) { // this skips the first one = header
				while ($bed_write == TRUE)	{	
					$rline = explode("\t",$line);
					//   keep first 6 columns of the bed_out file
					for($a=0; $a < 6; $a++) {	
						$kline[$a] = $rline[$a];
					}
					//$kline[6] = "\n";
					if (fputcsv($wf,$kline, "\t") == FALSE)	{	
						$bed_err[$bed_errcount++] = 'w_bed6';
						$bed_write = FALSE;
						break;
					}
					if (($line = fgets($rf)) == false)	{
						$bed_write == FALSE;
						break;
					}
				}
			}		
			else	{
				$bed_err[$bed_errcount++] = 'r_bedout';
				
			}						
			fclose($wf);
			if (filesize($fasta2bed) == 0)	 {
				$bed_err[$bed_errcount++] = 'nowf_fast2bedmt';
			}		
		}
		else 	{
			$bed_err[$bed_errcount++] = 'w_bedopen';
		}
	}
	else	{
		$bed_err[$b-+ed_errcount] = 'r_bedopen';
	}
	fclose($rf);
	if (empty($bed_err))	{
		//echo "no bed_err returning ;
	}
	else	{
		return($bed_err);
	}
}
if (empty($bed_err))	{
	echo "Successful conversion.";
	$fdisplay[0] = $userfa;	
	$fdisplay[1] = $fasta2bed;      
	for ($i=0; $i<2; $i++)	{
		if (!file_exists($fdisplay[$i]))	{
			echo "There is a problem in displaying the converted files.";
			$bed_err[$bed_errcount++] = 'fd_nofile' . $fdisplay[$i];
		}
	}
	$fdisplay[4] = $cpath . "/" . $user_file; 
	$fdisplay[5] = $cpath . "/" . $user_bed;
	$tempnme = "/" . $tempnme . "/";
	$rtempnme = '/' . $rtempnme . '/';
	$fdisplay[2] = $user_ffile;
	$fdisplay[3] = $user_bbed;			
}
else {
	echo "Error converting FASTA...";
}
return($bed_err);	
}			//End of fasta2bed main

function parse_sequence($tuserfa,$userfa, $fasta, &$type)	{
global $bed_err;
global $bed_errcount;
$header = 0;
$endol = "\r\n";   //windows, change for unix
$newhdrpos = 0;
$line = array();
$seq_to_parse = array();
$sequence = array();
$n = 0;
$k = 0;
$s = 0;
$h = 0;
$l = 0;
$c = 0;
$t_ct = array( 0, 0 );
$skip = 0;
$wfo = 0;

if (($fso = fopen($userfa, 'w')) == FALSE) {
			$bed_err[$bed_errcount++] = 'so_openuserseqfile';
			return($bed_err);
		}
		
if (!empty($tuserfa))	{ 
	if (!file_exists($tuserfa))	{
		$bed_err[$bed_errcount++] = 'no_userfile';
		return($bed_err);
		}
	else	{
		//parse file to check if it is a 
		//protein fasta or dna,rna fasta and sanitize the file
		if (($fo = fopen($tuserfa, 'r')) == FALSE) {
			$bed_err[$bed_errcount++] = 'userfile_openerr';
			echo 'Error opening ' . $tuserfile . '. Please check file.';
			return($bed_err);
		} 
		else	{
			while ($wfo == 0)	{
				if (($line = (fgets($fo))) == FALSE )	{
					$wfo = 1;
					break;
				}
				else	{
					$n = 0;
					// check for header
					if ($line[$n] == '>')	{
						$h = 1;
						$header = $line;
						$seq_to_parse = process_header($header,$n);
						if ($seq_to_parse == "skip_sequence") {
							$skip = 1;
							break;
						}
						if (!empty($seq_to_parse)) {
							$header = substr($header,0,$n);
							if (fwrite($fso, $header) == FALSE) {
								$bed_err[$bed_errcount++] = 'w_fastaoutfile1';
							}
							$type = sequence_parsing($seq_to_parse,$t_ct);
							if (fwrite($fso, $seq_to_parse) == FALSE) {
								$bed_err[$bed_errcount++] = 'w_fastaoutfile2';
							}
						}
						else	{
							if (fwrite($fso, $header) == FALSE) {
								$bed_err[$bed_errcount++] = 'w_fastaoutfile3';
							}
						}
					}	
					else 	{
						if ($skip == 1)	{
							break;
						}
						if ($h == 1)	{
							$type = sequence_parsing($line,$t_ct);
							if (fwrite($fso,$line) == FALSE) {
								$bed_err[$bed_errcount++] = 'w_fastaoutfile4';
							}		
						}					
					}
				}
			}
			if (!feof($fo)) {
				$bed_err[$bed_errcount++] = 'w_fofgetsfail';
				echo "There is a problem reading your FASTA file.";
			}
			fclose($fo);
		}
	}				
}

else {
	if (empty($fasta))  	{				
		echo "There is a problem processing your FASTA sequence.  Please check the sequence.";
		$bed_err[$bed_errcount++] = 'fs_nofaseq';
		return;
	}
	else	{
		$sequence = explode("\n", $fasta);
		$s = sizeof($sequence);
		for ($k=0;$k<$s; $k++)	{
			$line = ($sequence[$k]);
			$l = sizeof($line);
			for ($n=0;$n<$l; $n++)	{
				// check for header
				if ($line[$n] == '>')	{
					$skip = 0;
					$h = 1;
					$header = $line;
					$seq_to_parse = process_header($header,$n);
					if ($seq_to_parse == "skip_sequence") {
						$skip = 1;
						break;
					}
					if (!empty($seq_to_parse)) {
						$header = substr($header,0,$n);
						$header = $header . "\n";

						if (fwrite($fso, $header) == FALSE) {
							$bed_err[$bed_errcount++] = 'w_fastaoutfile5';
						}
						$type = sequence_parsing($seq_to_parse, $t_ct);
						$seq_to_parse = $seq_to_parse . "\n";
						if (fwrite($fso,($seq_to_parse)) == FALSE) {
							$bed_err[$bed_errcount++] = 'w_fastaoutfile6';
						}
					}
					else {
						$header = $header . "\n";
						if (fwrite($fso, $header) ==  FALSE) {
							$bed_err[$bed_errcount++] = 'w_fastaoutfile7';
						}
					}
				}
				else 	{
					if ($skip == 1)	{
						break;
					}
					if ($h == 1)	{
						$type = sequence_parsing($line, $t_ct);
						$line = $line . "\n";
						if (fwrite($fso, $line) == FALSE) {
							$bed_err[$bed_errcount++] = 'w_fastaoutfile8';
						}
					}
				}
			}
		}			
	}
}
if (filesize($userfa) == 0)	 {
		$bed_err[$bed_errcount++] = 'no_userfastamt';
	}
fclose($fso);

if (($t_ct[0] == 1) && ($type !== "-t=prot -q=prot")) {
	$type = "-t=prot -q=prot";
} 
return($bed_err);
}

function process_header($header, &$ch) {
	$long_header = 120;
	$endol = "\n";
	$w = 1;   			//eol length
	$hline = "";
	$k = 0;
	$l = strlen($header);
	$exclseq = array("<", "require","include", "open", "`", "~", "system","location", "eval","exec", "shell", "get","post","POST","GET","file_get_contents", "open" );
	$le = sizeof($exclseq);
	for ($k=0; $k<$le; $k++)	{
		if (strpos($header,$exclseq[$k]) == TRUE)	{
			echo "Sequence header contains invalid characters.  Please check header. Skipping sequence.";
			$hline = 'skip_sequence';
			return($hline);
		}
	}
	for ($i=$ch; $i<$l; $i++)	{
		if ($header[$i] == $endol)	{		
			if ($i < ($l-$w))	{
				$hline = substr($header,$i);
				return($hline);
				break;
			}
			else	{
				return($hline);
			}
		}
		if ($i > ($long_header + 1)){
			$hline = substr($header,$i);
			$ch = $i;
			echo "FASTA header found to be longer than " . $long_header . ". Check to make sure the header is a single line ending with an end-of-line character. Please check file before re-submitting.";
			return($hline);
			break;
		}			
		
	}
	return($hline);
}

function sequence_parsing(&$strg, &$t_ct)	{
global $bed_err;
global $bed_errcount;
$validdna = 'acgtnx-';
$validrna = 'u';
$plist = 'rdeqhilkmfpswyv-*';
$onestrg = $strg;
$onechar = str_split($strg);
$x = sizeof($onechar);
$i = 0;
$not_found = 0;
$rep = 'N';
$n = 0;
$r = 0;
$h = ">";
$prot = 0;
$type = "";   //default is dna

for ($i; $i < $x; $i++) {
	if (stripos($plist,$onechar[$i]) !== FALSE)	{
		$prot++;
	}
	else	{
		if (stripos($validdna,$onechar[$i]) !== FALSE){
			$n++;
		}
		else	{
			if (stripos($validrna,$onechar[$i]) !== FALSE) {
				$r++;
			}
			else {
				if ($onechar[$i] == $h) {
					//Writing the rest of sequence to file
					$strg = substr($onestrg,0,$i);  
					$header = substr($onestrg,$i); 
					// FIX this, need to process and write header without creating a potential infinite loop
					// for now, the user needs to clean this up in the FASTA sequence file
					$bed_err[$bed_errcount++] = 'Header_inside_sequence'; 
					echo "Alert: FASTA header found in the middle of sequence line. Please check to make sure FASTA header is a single line description.";
				}
				$strg[$i] = $rep;   //*****char not found in valid dna, rna, or protein, replace char in string with unknown N
				$not_found++;
			}
		}
	}
}
$d = $n + $r;	
if ($prot == 0)	{
	if ( $t_ct[0] == 1 ) {
		if ( $d > $t_ct[1] )	{
			$t_ct[0] = 0;
			$t_ct[1] = $d;
		}
	}
	else {
		$t_ct[1] = $t_ct[1] + $d;
	}
	if ($r > 0) {
	$type="-t=dna -q=rna";
	}
	else	{
		if ($n > 0)	{
			//default is dna
		}
		else {      //dna and rna nucleotide count = 0 and protein count = 0
			echo "FASTA is not valid. Please check it and try again";
			$type="not_valid";
			$bed_err[$bed_errcount++] = 'No_validchars';
		}
	}
}
else {
	$type="-t=prot -q=prot";
	$t_ct[0] = 1;
	$t_ct[1] = $t_ct[1] + $prot;
}
return $type;
}

function get_name($tfiles, &$tempnme)	{
	$n = $tempnme = $tstr = "";
	$sl = '/';
	$dot = ".";
	$str = "php";
	$i = $ni = $di = $len = 0;
	
	if (empty($tfiles)) {
		$tempnme = $tfiles;
	}
	else	{
		if (($i = strrpos($tfiles,$sl)) == TRUE)	{
			$tstr = substr($tfiles, $i);
			if (($ni = strrpos($tstr, $str)) == TRUE ) { //php
				if (($di = strrpos($tstr, $dot)) == TRUE ) {
				}	
				else	{
					$di = strlen($tstr);
				}
				$len = $di - ($ni+3);
			}
			else  {
				$di = strlen($tstr);
				$len = $di - ($i+1);
			}
			if (($n = substr($tstr, ($ni+3), $len)) == TRUE ) {
				$tempnme = $n . "_";
			}	
			else {
				$tempnme = "xxxx_";
			}
		}
	}
	return;
}

function check_ufile($formname, &$p_faseq, &$fasta, $newname) {
global $bed_err;
global $bed_errcount;
	
	$newname = "";
	if (($fasta = $_POST["fasta"]) !== FALSE) {		//FASTA sequence(s) pasted into fastaform.html.twig	
		if (!empty($fasta))	{
			$p_faseq = 1;
		}
	}
	if ($_SERVER['CONTENT_LENGTH'] > 2000000)	{
		$bed_err[$bed_errcount++] = 'fe_ovrmaxsize';
		echo "The FASTA file exceeds the maximum file size.";
		$newname = "1";
		return($newname);			
	}
	if (isset($_FILES[$formname]['error']))	{
		if (is_array($_FILES[$formname]['error'])) {
			$bed_err[$bed_errcount++] = 'fe_noerrsetormult';
			echo "Multiple FASTA files found. Only one file can be processed.  Please check file specified.";
			$newname = "1";
			return($newname);
			}
		if ($_FILES[$formname]['error'] == UPLOAD_ERR_FORM_SIZE)  {
			$bed_err[$bed_errcount++] = 'fe_ovrmaxsize';
			echo "The FASTA file exceeds the maximum file size of 2MB.";
			$newname = "1";
			return($newname);
		}	
		if ($_FILES[$formname]['error'] == UPLOAD_ERR_INI_SIZE)  {
			$bed_err[$bed_errcount++] = 'fe_ovrmaxsize';
			echo "The FASTA file exceeds the maximum allowable file size.";
			$newname = "1";
			return($newname);
		}
		if ($_FILES[$formname]['error']  == UPLOAD_ERR_PARTIAL)  {
			$bed_err[$bed_errcount++] = 'fe_ovrmaxsize';
			echo "The FASTA file was only partially uploaded.";
			$newname = "1";
			return($newname);
		}
		if ($_FILES[$formname]['error'] == UPLOAD_ERR_NO_FILE)	{
			if ($p_faseq == 0) {
				$bed_err[$bed_errcount++] = 'fe_nofile';
				echo "There was a problem uploading your FASTA file. Please check your file or pasted sequences.";	$newname = "1";
				return($newname);
			}
		}
	}
	if (($newname = $_FILES[$formname]['name']) == FALSE)  {	
		if ($p_faseq == 0)  {
			$bed_err[$bed_errcount++] = 'fn_nameerr';
			echo "There was a problem with your FASTA file name. Please check the name.";
			$newname = "1";
			return($newname);
		}
	}
	if (empty($newname)) {
		if ($p_faseq == 0) {
			$bed_err[$bed_errcount++] = 'fn_noname';
			echo "There was a problem loading your FASTA sequence. Please check your file name or pasted sequence.";
			$newname = "1";
			return($newname);
		}			
	}
	else {
		$newname = trim($newname);
		$newname = stripslashes($newname);
		$newname = htmlspecialchars($newname);
		$newname = preg_replace("([^\w\s\d\-_.])", '_', $newname);
	}
return($newname);	
}

function check_user_type($type)	{
$ut = "";
$protein = "-t=prot -q=prot";
$rna = "-t=dna -q=rna";
	
	if (($user_type = $_POST["ftype"]) == FALSE) {	
		$user_type = "DNA";
		}
	if ($user_type == "Protein")	{
		$ut = $protein;
		if ($type !== $protein)	{
			echo "<script>";
			echo 'alert("FASTA not found to be protein and will be processed as DNA/RNA.")';
			echo '</script>';
		}
	}
	else if ($user_type == "RNA")	{
		$ut = $rna;
		if ($type == $protein)	{
			echo "<script>";
			echo 'alert("FASTA found to be protein and will be processed as protein.")';
			echo '</script>';	
		}
	}	
	return;
}

function format_table($filename)	{
	if( ($handle = fopen( $filename, 'r' )) == FALSE )	{
		$bed_err[$bed_errcount++] = 'bd_nobedtodisp';
	}
	$data = fgetcsv( $handle );
	$data[0] = preg_replace('/:"/', ':', $data[0]);
	$data[0] = preg_replace('/"for/', 'for', $data[0]);
	$output .= '<div class="info_box tfb">';
    $output .= '<table id="fdisplay">';
	$output .= sprintf( '<tr class="tfh1">%s</tr>', $data[0] );
    while( ($data = fgetcsv( $handle )) !== false )
    {
		$output .= '<tr class="tfr1">';
		foreach( $data as $y )
		{
			$x = explode( "\t", $y );
			$output .= sprintf( '<td class="tfd0">%s</td>', $x[0] );
			$output .= sprintf( '<td class="tfd1">%s</td>', $x[1] );
			$output .= sprintf( '<td class="tfd2">%s</td>', $x[2] );
			$output .= sprintf( '<td class="tfd3">%s</td>', $x[3] );
			$output .= sprintf( '<td class="tfd4">%s</td>', $x[4] );
			$output .= sprintf( '<td class="tfd5">%s</td>', $x[5] );			
		}
		$output .= '</tr>';		
    }
    fclose( $handle );
    $output .= '</table>';
	$output .= '</div>';

	echo $output;
	return;
}

?>
</div> <!--cr_format-->
</div> <!-- class f 
{% endblock %}

{% block footer %}

{% endblock %} --->
</body>
</html>