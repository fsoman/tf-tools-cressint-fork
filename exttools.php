<?php
// ========================================================================
//
//      CressInt cresstoolsportals.php
//
//  Authors:   K. Ernst, F. Soman
// 
//
// ========================================================================

require_once('/var/www/lib/php/WRL/tfinit.php');

$cfg = tf_init();

global $TWIGVARS;  // the tf_template_init*'s create this variable hash
global $TWIG;      // ...and this Twig environment

$TWIGVARS['gbtrackid'] = $cfg['ucsc']['custom_track_id'];

// ...so that all we have to do is call the 'render' method
echo $TWIG->render('exttools.html.twig', $TWIGVARS);

// vim: set tags+=/var/www/lib/php/WRL/tags
