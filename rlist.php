<?php
// ====================================================================
//
//   CressInt's 'rlist.php'
//
//   Display a list of completed jobs in the browser, each of which
//   is clickable and opens a complete display in 'display.php'.
//
//   Authors: Borowczak, M., Dixit, S., Ernst, K.
//
// ====================================================================
require_once('../../lib/php/WRL/tfconfig.php');
require_once('../../lib/php/WRL/tftemplate.php');
require_once('../../lib/php/WRL/tfresults.php');
require_once('../../lib/php/WRL/tfcache.php');

//ini_set('display_errors', 'On'); // uncomment for debugging in-browser
$tool_name = 'cressint';
$ini_file  = $tool_name . '.ini';


// ------------------------------------------------------------------------
//          Load config settings from the .ini file for this tool
// ------------------------------------------------------------------------
$cfg = NULL;
if (!file_exists($ini_file)) tf_bail("ACK! Missing or corrupted $ini_file.");
if (!tf_read_config($cfg, $ini_file)) tf_bail('Problem reading $ini_file');

// FIXME: This is not a reliable way to do this. What we really want to do is 
// parse out at least this one line in the config file for *every* entry in 
// the results table, or else have *everything* we need in the (JSON)
// "job.properties" file.
$log_subdir    = basename($cfg['cluster']['bsub_log_path']); // e.g., 'bsub_logs'
$proper_name   = $cfg['global']['proper_name'];
$job_prop_file = $cfg['global']['job_props'];
$cache_file    = "./templates/cache/results_table.html";

$files         = split(',', $cfg[$tool_name]['output_files']);
$results_file  = $files[0];

// ------------------------------------------------------------------------
//                      Initialize template system
// ------------------------------------------------------------------------

// This (sadly) is the calling convention for the present version of 
// tftemplate.php:
global $TEMPLATE; $TEMPLATE = 'tfresults.html.twig';
global $TWIGVARS;
$TWIGVARS = array(
  'toolname'   => $tool_name,
  'propername' => $proper_name,
  'pagetitle'  => "$proper_name job results",
  'headerh1'   => <<<END_OF_HTML
  <a href="/$tool_name" title="Back to TF Tools $proper_name">$proper_name</a> results
END_OF_HTML
  ,'results'   => ''
);

tf_template_init();  // Initialize Twig renderer

// ------------------------------------------------------------------------
//                     Check job output directory
// ------------------------------------------------------------------------

// If the output path is based on a job ID, we'll enumerate the folders in the 
// *parent* directory. FIXME: Find a better way to do this:
$output_path   = $cfg[$tool_name]['output_path'];
//echo "<pre>"; print_r($cfg); echo "</pre>\n";
// FIXME: Refactor
$output_path   = preg_replace('|/[^/]+/?$|', '', $output_path);
if (!is_dir($output_path)) {
  tf_bail("Job output directory \"$output_path\" for $proper_name "
          . "doesn't exist or isn't readable.");
}


// ------------------------------------------------------------------------
//      Check cache freshness or else update the cache; render results
// ------------------------------------------------------------------------
if (tf_cache_is_fresh($cache_file, $output_path)) {

  tf_render_results( "<!-- retrieved from cache -->\n"
                     . file_get_contents($cache_file) );

} else {

  $err = '';
  $success = tf_update_results_cache( array( 'cache_file'    => $cache_file,
                                             'output_path'   => $output_path,
                                             'results_file'  => $results_file,
                                             'proper_name'   => $proper_name,
                                             'job_prop_file' => $job_prop_file,
                                             'output_path'   => $output_path,
                                             'output_path'   => $output_path,
                                           ), $err );
  if ($success) {
    // This function already knows which template to use (the one for "results')
    tf_render_results(file_get_contents($cache_file));
  } else {
    tf_bail("Problem updating results cache (details: $err).");
  }

} // if the cache file is fresh

// End of rlist.php
