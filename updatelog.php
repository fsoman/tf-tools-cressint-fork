<?php
// ========================================================================
//
//  CressInt  updatelog.php
//
//  Authors:   K. Ernst, F. Soman
// 
//
// ========================================================================

require_once('/var/www/lib/php/WRL/tfinit.php');
require_once('/var/www/lib/php/WRL/tftemplate.php');

$ini_file = tf_find_ini();  // open toolname.ini in c.w.d.


if ( !tf_template_init_from_ini($ini_file, $form_key) ) {
  tf_bail("Problem reading config file \"$ini_file\" and/or rendering index.");
}

// Otherwise
// FIXME: tftemplate.php really should have a function like 
// tf_render_template('templatename'); here we have to call Twig manually. 
// Usually, functions from 'tftemplate.php' are abstracting this messy work 
// away.
global $TWIGVARS;  // the tf_template_init*'s create this variable hash
global $TWIG;      // ...and this Twig environment

// ...so that all we have to do is call the 'render' method
echo $TWIG->render('updatelog.html.twig', $TWIGVARS);


// vim: set tags+=/var/www/lib/php/WRL/tags
