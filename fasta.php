<?php
//*******************************************************************************//
//                                                                               //
//	CressInt Arabidopsis thaliana Intersector 
//  fasta.php   FASTA User input from Cressint homepage Fasta button
//				This is the php executable to send user FASTA input from
//				the Cressint homepage to process in the fasta to BED 
//              conversion function, f2b
//
//    Author:  Frances Soman 
//*******************************************************************************//
// Use:		fasta.php --f 'user_fasta_file.fa' --s 'fasta'
// Output:	BED 6 file converted from the user's fasta file or sequence

include fasta2bed.php;
{
$no = 0;
$fastafile = "";
$fasta = array();
$calledby = 1;


$no = $_SERVER["argc"];
$arg_f = "";

for ($c=0; $c<$no; $c++)	{
	if (($arg_f = $_SERVER["argv"][$c]) == '--f') {		//FASTA file submitted
		++$c;
		$arg_f = $_SERVER["argv"][$c];
		if (file_exists($arg_f) == FALSE)	{
			echo 'FASTA file not found';
			return();
		}
		else	{
			$fastafile = $arg_f;
			$c = $no;
		}
	}
	else 	{		
		if ($arg_f == '--s')	{	//FASTA sequence array submitted
			++$c;
			$arg_f = $_SERVER["argv"][$c];
			if (empty($arg_f))	{
				echo 'FASTA sequence not specified';
			}
			else {
				$fasta = $arg_f;
				$c = $no;
			}
			
		}
	}
}

// Call the fasta 2 BED function, f2b
f2b($fastafile, $fasta, $calledby);

}






?>